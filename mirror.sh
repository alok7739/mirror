#!/bin/sh

set -euxo pipefail

NAME=$1
SOURCE_URL=$2
TARGET_URL=$3

if [[ -d "$NAME.git" ]]; then
    cd "$NAME.git"
    git fetch --prune --tags --prune-tags "$SOURCE_URL" "+refs/*:refs/*"
else
    git clone --bare "$SOURCE_URL"
    cd "$NAME.git"
fi

git push --mirror "$TARGET_URL"
